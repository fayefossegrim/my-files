#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc
source ~/.bashrc
source ~/.bash_login
case "$OS" in
  IRIX)
    stty sane dec
    stty erase
    ;;
#  SunOS)
#    stty erase
#    ;;
  *)
    stty sane
    ;;
esac
