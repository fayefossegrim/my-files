# in Ruby metodele,variabilele si fisierele
# sunt scrise in snake_case fomatting
# cuvinte fara majuscula separate cu _

# Naming a file
this_is_snake_cased_file.rb

# Assigning a variable
forty_two = 42

# Defining a method
def this_is_a_great_method
# do stuff
end

# O variabila constanta se defineste NUMAI cu majuscule

FOUR = 'four'
FIVE = 5


# Multi-line
[1, 2, 3].each do |i|
  #do stuff
end

# Does the same thing in one line
[1, 2, 3].each { |i| do_some_stuff }

# Creeare clasa
class MyFirstClass
end

class MySecondClass
end
